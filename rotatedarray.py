
def print_top_right_layer(l,x1,y1,x2,y2):

    for i in range(y1,y2):
        print l[x1][i],

    for i in range(x1,x2):
        print l[i][y2],
    if x2-x1 > 0:
      print_bottom_left_layer(l,x1+1,y1,x2,y2-1)


def print_bottom_left_layer(l,x1,y1,x2,y2):


    for i in range(y1,y2,-1):
        print l[x2][i]

    for i in range(x1,x2,-1):
        print l[i][y1]

    if y2-y1>0:
      print_top_right_layer(l,x1,y1+1,x2-1,y2)
   



def printrotated(l,maxrow,maxcol):
    currentrow = 0
    currentcol = 0
    m = maxrow
    n = maxcol
    while currentrow<m and currentcol<n:
      # print row upto col -1
      for i in range(currentcol,n):
        print l[currentrow][i]
      currentrow+=1
      #print last col of all rows, upto last but one row
      for i in range(currentrow,m):
        print l[i][n-1]
      n-=1
      
      if currentrow < m:
        #print last row backwards from maxcol-1 to first col
        for i in range(currentcol,n-1,-1):
          print l[m][i]
        m-=1
      #print first column of all 
      #if currentcol <= n:
      for i in range(currentrow,m,-1):
        print l[i][currentcol]
      currentcol+=1


def main():
  print "array"

  l=[[1,2,3],
     [8,9,4],
     [7,6,5]
     ]

  maxrow=len(l)
  maxcol=len(l[0])

  print maxrow,maxcol

  printrotated(l,maxrow,maxcol) 
 






if __name__ == "__main__":
    main()
