def switch_case(letter):
    if letter.isupper():
        return letter.lower()
    else:
        return letter.upper()

def get_combinations(string):
    #print string
    if len(string) <=1:
        yield string
    for letter in string:
        #print letter
        for x in get_combinations(string[1:]):
            if letter.isalpha():
                yield switch_case(letter) + x
            yield letter + x






# you get this (blackbox, can't modify)
def decode(testEncStr):
    if testEncStr == "_KLjjjhhhHJKs_":
        return 848662
    else:
        return None # Return None if not found

# you implement this
def decodeFind(badEncStr):
    for string in get_combinations(badEncStr):
        print string
        result = decode(string)
        if result:
            return result
    return None

#decodeFind("kljJJ324hjkS_")
print "Decoded id: " + str(decodeFind("_KLJJJhhHHJKS_"))