

def cartesian_product(L,*lists):
  if not lists:
    for x in L:
      yield (x,)
  else:
    for x in L:
      for y in cartesian_product(lists[0],*lists[1:]):
        yield (x,)+y



def all_perms(str):
  if len(str) <=1:
      yield str
  else:
      for perm in all_perms(str[1:]):
          for i in range(len(perm)+1):
              #nb str[0:1] works in both string and list contexts
              yield perm[:i] + str[0:1] + perm[i:]

def comblen(seq, n):

    if n==0:
        yield []
    else:
        for i in range(len(seq)):
            for item in comblen(seq[i+1:],n-1):
                yield [seq[i]]+ item


def powerset(seq):
  """
  Returns all the subsets of this set. This is a generator.
  """
  if len(seq) <= 1:
    yield seq
    yield []
  else:
    for item in powerset(seq[1:]):
      print "seq[0]",seq[0]
      yield [seq[0]]+item
      print "item",item
      yield item

def all_perms_lol(lists, previous_stuff = []):
  
  if len(lists) == 1:
    for item in lists[0]:
      yield previous_stuff + [item,]

  else:
    for elem in lists[0]:
      for x in all_perms_lol(lists[1:], previous_stuff + [elem,]):
        yield x



def main():
   l1 = [1,2]
   l2 = ["kara","z"]
   l3 = ["fire","belly"]
   l4 = ['f']
   l5 = ['@','a']
   l6 = ['c','2']
   print cartesian_product(l1,l2)
   for item in cartesian_product(l4,l5,l6):
     print item
     pass

   for item in all_perms(l1):
     #print item
     pass
   print "all combs?"
   biglist = []
   biglist.append(l1)
   biglist.append(l2)
   biglist.append(l3)
   for item in all_perms_lol(biglist):
     print item

   print "Powerset"
   for item in powerset(l1):
     pass #print item
   print "COmb of len"
   x = [1,2,3,4,5]
   x = comblen(x,2)
   for item in x:
       print item







if __name__ == "__main__":
  main()
