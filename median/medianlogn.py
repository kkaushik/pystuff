#Find median of two sorted arrays in log n , with and without doing the n way
#merge

#general code to select kth smallest element from two lists a and b of length
#sa and sb

def select(a, b ,sa, sb, k):
    if sa < k/2:
        ma = sa-1
    else:
        ma = k/2-1

    mb = k - ma -2

    if (sa+sb) < k:
        return -1
    if sa == 0:
        return b[k-1]
    if sb== 0:
        return a[k-1]
    if k==1:
        if a[0] < b[0]:
            return a[0]
        else:
            return b[0]
    if a[ma] < b[mb]:
        return select(a+[ma]+[1],b,sa-ma-1,mb+1,k-ma-1)
    return select(a,b+[mb]+[1],ma+1,sb-mb-1,k-mb-1)

#median case

def medianofsortedarrays(a,b,sa,sb):
    m1=0
    m2=0
    if (sa+sb) % 2 == 1:
        return select(a,b,sa,sb,((sa+sb)/2)+1)
    m1 = select(a,b,sa,sb,(sa+sb)/2)
    m2 = select(a,b,sa,sb,(sa+sb)/2+1)
    return (m1+m2) /2 









print medianofsortedarrays([1,2,3],[6,7,8],5,5)


