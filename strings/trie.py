class Node: 
  def __init__(self, letter=None, isTerminal=False): 
    self.letter=letter
    self.children={}
    self.isTerminal=isTerminal

class Trie:

    def __init__(self):
        self.root = Node('')

    def insert(self,word):
        current=self.root 
        for letter in word: 
            if letter not in current.children:
                current.children[letter]=Node(letter)
            current=current.children[letter] 
        current.isTerminal=True

    def __contains__(self,word):
        current=self.root 
        for letter in word: 
            if letter not in current.children:
               return False  
            current=current.children[letter] 
        return current.isTerminal
   


def main():
  print "array"

  l=[[1,2,3],
     [8,9,4],
     [7,6,5]
     ]

  x = Node("A")
  y = Trie()
  y.insert("HELLO")
  print "HELLO" in y






if __name__ == "__main__":
    main()
