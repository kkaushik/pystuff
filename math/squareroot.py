#Find square root of a number given a precision
#Newton rhapson method, do binary search  shrink range till precision 
#is satisfied
import decimal


def root(a,n):
    #a = 1.0
    tol = 2.4e-20
    x = a
    xprev = 0
    if a < 0 or n<=0: return -1
    while xprev - x > tol or x - xprev > tol:
        xprev = x
        x = xprev*(n-1 + a/xprev**n)/n
    return x


def new_sq_root(num):
    left = 0
    right = num
    tol = 0.0000001
    sq = 0
    while (right-left > tol):
        sq = (right + left)/2
        print sq
        if (sq * sq) < num:
            left = sq
        else:
            right = sq
    return sq



def sqrt(x):
    a = 1.0
    for _ in range(100):
        a= (a+ x/a)/2
    return a

r = sqrt(11)
print r
print root(48,2)
print r == decimal.Decimal(11).sqrt()

print "ANS",new_sq_root(64.0)











