def numlessthan(numStr):
  if (len(numStr) == 2 and int(numStr[0]) < int(numStr[1])):
    return False
  splitstr = [x for x in numStr]
  # find lowest number less than me
  allexceptme = [u for u in splitstr if int(u) < int(splitstr[0])]
  if allexceptme:
    firstnum = sorted(allexceptme,reverse=True)[0]
    return [firstnum] + sorted([u for u in splitstr if u != firstnum],reverse=True)
  else:
    result = numlessthan(splitstr[1:])
    if result:
          return [splitstr[0]] + result
    else:
          return False

print numlessthan('123412')
