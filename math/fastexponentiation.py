#To compute exponentiate a number recursively


def exponentiate(a, n):
    if n ==0:
        return 1
    if n==1:
        return a
    if n%2 == 1:
        return  a * exponentiate(a,n-1)
    else:
        p = exponentiate(a, n/2)
        return p * p

def mypow(x , n):
	res = 1
	while (n):
		if n & 1:
			res *=x
		n >>=1
		x +=x
	return res




print exponentiate(3,200)
print "normal way below"
print 3**200
print "my power"
print mypow(3,200)



