


class Bldg:
    def __init__(self,x1,h1,x2):
        self.lx = x1
        self.h =  h1
        self.rx = x2

class Skyline:

    def __init__(self,n):
        #initialize an empty list of strips
        self.strips =  [[0,0]] * n
        self.count = 0
        self.startLoc=0
    
    def head(self):
        return self.strips[self.startLoc]

    def append(self, strip):
        self.strips[self.startLoc+self.count] = strip
        self.count+=1

    def removeHead(self):
        start = self.strips[self.startLoc]
        self.count -=1
        self.startLoc +=1
        return start

    def printSky(self):
        for i in range(self.startLoc,self.startLoc+self.count):
            print self.strips[i]



def MergeSkyline(sk1, sk2):
    SK = Skyline(sk1.count + sk2.count)
    curh1 = 0
    curh2 = 0
    while( sk1.count>0 and sk2.count>0):
        if sk1.head()[0] < sk2.head()[0]:
            curx = sk1.head()[0]
            curh1 = sk1.head()[1]
            maxh = curh1
            if curh2> maxh:
                maxh = curh2
            SK.append([curx,maxh])
            sk1.removeHead()
        else:
            curx = sk2.head()[0]
            curh2 = sk2.head()[1]
            maxh = curh1
            if curh2>maxh:
                maxh = curh2
            SK.append([curx,maxh])
            sk2.removeHead()

        while sk1.count > 0:
            SK.append(sk1.removeHead())
        while sk2.count > 0:
            SK.append(sk2.removeHead())
        return SK








def findSkyline(B, lo ,hi):
    if lo==hi:
        sk = Skyline(2)
        sk.append([B[lo][0], B[lo][1]])
        sk.append([B[lo][2], 0])
        return sk

    mid = (lo+hi)/2
    sk1 = findSkyline(B, lo, mid)
    sk2 = findSkyline(B, mid+1, hi)
    return MergeSkyline(sk1,sk2)




B = [[1,11,5],
        [2,6,7],
        [3,13,9],
        [12,7,16],
        [14,3,25],
        [19,18,22]

        ]

sk = findSkyline(B,0,5)
sk.printSky()







input = [[1,11,5], [2,6,7], [3,13,9], [12,7,16] ,[14,3,25] , [19,18,22] ]
#op = []



