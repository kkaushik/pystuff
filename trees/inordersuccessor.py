#Determine the inorder successor of a binary search tree

#from treebasics import Node
#from treebasics import traverse


class Node:
    lft = None
    rgt = None
    parent = None
    def __init__(self, key, val):
        self.key = key
        self.val = val




def inordersuccessor(node):

    if node.rgt:
        #go to left most child of this node
        x = node.rgt
        if x.lft:
            while(x.lft):
                x = x.lft
        return x.val
    else:
        #Move up parents to find a parent for whom node is a left child
        print "to be done"
        currparent = node.parent
        while currparent and node == currparent.rgt:
            node = currparent
            currparent = currparent.parent
        return currparent.val




#x = Node(2,2)

#build the tree here
x = Node(5,5)
x.parent = None
x.lft = Node(3,3)
x.lft.parent = x
x.lft.lft = Node(1,1)
x.lft.lft.parent = x.lft
x.rgt = Node(6,6)
x.rgt.parent = x
#traverse(x)

print inordersuccessor(x.lft.lft)



