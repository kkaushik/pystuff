#Level order traversal of BST
from collections import deque


def BFS(G, s):
    P, Q = {s:None}, deque([s])
    while Q:
        u = Q.popleft()
        for v in G[u]:
            if v in P: continue
            P[v] = u
            Q.append(v)
    return P


def BFS_levelorder(G, s):
    P, currentlevel = {s:None}, deque([s])
    nextlevel = deque()
    print currentlevel[0], "\n"
    while currentlevel:
        u = currentlevel.popleft()
        for v in G[u]:
            if v in P: continue
            P[v] = u
            print v,
            nextlevel.append(v)
        if not currentlevel:
            currentlevel, nextlevel = nextlevel, currentlevel
            print "\n"
    return currentlevel

def DFS_levelorder(G, level):
    if not G: return
    if level == 1:
        print G[0], " "
    else:
        for item in G[
        DFS_levelorder(

G = {'a' : ['b','c'],
        'b': ['d','e'],
        'c': ['f','g'],
        'd': [],
        'e': [],
        'f': [],
        'g': []
        }


print BFS_levelorder(G, 'a')
