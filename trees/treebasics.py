#Basic representation of a tree with traversing it and searching it
from collections import deque



class Node:
    lft = None
    rgt = None
    def __init__(self, key, val):
        self.key = key
        self.val = val

def insert(node, key, val):
    if node is None: return Node(key,val)
    if node.key == key: node.val = val
    elif key < node.key:
        node.lft = insert(node.lft, key, val)
    else:
        node.rgt = insert(node.rgt, key, val)
    return node


def traverse(node):
    if node: print  node.val
    if node and node.lft:
        traverse(node.lft)
    if node and node.rgt:
        traverse(node.rgt)


def traverse_preorder(node):
    if node: print  node.val,
    if node and node.lft:
        traverse(node.lft)
    if node and node.rgt:
        traverse(node.rgt)

def traverse_gen_preorder(node):
    if node: 
        yield node.val
    if node.lft:    
      for child in traverse_gen_preorder(node.lft):
          yield child
    if node.rgt:
      for child in traverse_gen_preorder(node.rgt):
          yield child


def traverse_inorder(node):
    if node and node.lft:
        traverse(node.lft)
    if node: print  node.val,
    if node and node.rgt:
        traverse(node.rgt)


def traverse_postorder(node):
    if node and node.lft:
        traverse(node.lft)
    if node and node.rgt:
        traverse(node.rgt)
    if node: print  node.val



def traverse_levelorder1(node, Q=None):
    if Q==None: Q = deque([node])
    while Q:
        curnode = Q.popleft()
        if curnode.lft: Q.append(curnode.lft)
        if curnode.rgt: Q.append(curnode.rgt)
        print curnode.val


def traverse_levelorder(node, L=None):
    if L==None: L = [node]
    while L:
        nxtL = []
        for n in L:
            if n.lft: nxtL.append(n.lft)
            if n.rgt: nxtL.append(n.rgt)
            print n.val,
        L = nxtL
        print 




def verifybst(node,low, high):
    if not node: return True
    if low < node.val and node.val < high:
        return (verifybst(node.lft, low, node.val) and verifybst(node.rgt,
            node.val,high))
    else:
        return False


def lca(node, p, q):
    if not node or not p or not q:
        return None
    if max(p.val,q.val) < node.val:
        return lca(node.lft,p,q)
    elif min(p.val,q.val) > node.val:
        return lca(node.rgt,p,q)
    else:
        return node.val



def height(node):
    if not node:
        return 0
    else:
        maxht = max(height(node.lft),height(node.rgt))
        return maxht + 1

def maxpath(node):

    if node == None:
        return 0,0
    else:
        maxlftpath, lftht = maxpath(node.lft)
        maxrgtpath, rgtht = maxpath(node.rgt)
        maxpathtemp = lftht+ rgtht + 1
        maxht = max(lftht, rgtht)
        return max(maxpathtemp,maxht) , maxht +1


def getkthNode(node, k):
    if node.lft:
        val = getkthNode(node.lft, k)
        if val: return val
    k-=1
    if k==0:
        return node
    if node.rgt:
        val = getkthNode(node.rgt, k)
        if val: return val
    return None
    


def search(node, key):
    if not node: false
    if node.key == key: return node.val
    if node.key > key:
        return search(node.lft, key)
    else:
        return search(node.rgt, key)


def lcabinarytree(node, p ,q):
    if not node: return None
    if node.val == p.val or node.val==q.val: return node
    left = lcabinarytree(node.lft, p, q)
    right = lcabinarytree(node.rgt, p,q)
    if left and right: return node.val
    if left: return left
    if right: return right


def buildtree2(inputlist, min, max):
    if len(inputlist) < 1:
        return None
    val = inputlist[0]
    if min < val and val < max:
        nextval = inputlist.pop(0)
        node = Node(nextval,nextval)
        node.lft = buildtree2(inputlist,min, val)
        node.rgt = buildtree2(inputlist,val, max)
        return node
    else:
        return None









tree = Node(5,5)


for i in range(10):
    if i == 5:
        continue
    #print "inserted",i
    tree = insert(tree,i,i)
    #print "root is" , tree.val

# Traverse the tree
#traverse(tree)

#tree = Node(1,1)
#tree.lft= Node(2,2)
#tree.lft.lft= Node(4,4)
#tree.lft.lft.lft = Node(6,6)
#tree.rgt = Node(3,3)
#tree.lft.rgt = Node(5,5)
#tree.lft.rgt.rgt = Node(8,8)
#tree.lft.rgt.rgt.rgt = Node(9,9)
#tree.lft.rgt.rgt.rgt.rgt = Node(100,100)


#tree.lft.lft.rgt = Node(7,7)
#tree.lft.lft.rgt.rgt = Node(10,10)
#search for some stuff
#print "search"
#print search(tree, 2)

for i in range(10):
    if i == 5:
        continue
    print "inserted",i
    tree = insert(tree,i,i)
    #print "root is" , t

#print "preorder"
#print traverse_preorder(tree)


print "inorder"
print traverse_inorder(tree)
print "end of inorder"
print "kth node 4"
print getkthNode(tree,4).val
print "height"
print height(tree)
print "maxpath"
print maxpath(tree)

#print "postorder"
#print traverse_postorder(tree)


#print "level order"
#print traverse_levelorder(tree)

#print lca of two nodes:
print lca(tree,Node(4,4), Node(9,9))
print "binary tree lca"


badtree = Node(10,10)
badtree.lft = Node(4,4)
badtree.rgt = Node(15,15)
badtree.rgt.lft = Node(11,11)
badtree.rgt.rgt = Node(20,20)

print lcabinarytree(badtree,Node(20,20), Node(4,4))
for i in range(10):
    if i == 5:
        continue
    print "inserted",i
    tree = insert(tree,i,i)

print "test generator"
items = [item for item in traverse_gen_preorder(tree)]

print items
print "end generator"

print len(items)
#deserializing tree
print "deserialize"

nodez = buildtree2(items, float("-inf"), float("inf"))

print [item for item in traverse_gen_preorder(nodez)]

#print "verify bst"
#print verifybst(badtree, float("-inf"),  float("inf"))
#print "Create a tree and add a few nodes to it"
