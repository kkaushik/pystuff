#Number of binary search trees that can be constructed out of a given in-order traversal size Number





def getTrees(N):

	if N <=1:
		return 1
	sum=0
	for num in range(1,N+1):
		sum +=getTrees(num-1) * getTrees(N-num)
		
	return sum




print getTrees(4)
