#given a set of intervals, merge an interval into this set of intervals

#input = [[1,5], [6, 13], [20, 21], [23, 26], [27, 30], [35, 40]]
#merge = [14, 33]
input = [[1,4], [6,10], [14, 19]]
merge = [13,17]
highmerge = False
lowmerge = False
mergedlist = []
for item in input:
  if item[0] > merge[0] and item[1] < merge[1]:
    continue
  if item[1] < merge[1] and item[0] < merge[0]:
    item[1]=merge[1]
    highmerge = True
  if item[1] > merge[1] and item[0] > merge[0]:
    item[0] = merge[0]
    lowmerge = True
    
  mergedlist.append(item)
  
if not highmerge and not lowmerge: mergedlist.append(merge)

print mergedlist
