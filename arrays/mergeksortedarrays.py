#Merging k sorted lists in Python
# we maintain a heap of size k, then add head element of each list to heap
# After getting min from heap, we push element from that list into heap
from heapq import *

heap = []

#heappush(heap,(1,2))
#print heappop(heap)



lists = [ [1,3,4],
          [2,6],
          [5]]



for i in range(len(lists)):
	print lists[i][0]
	if len(lists[i]) > 0:
		heappush(heap,(lists[i].pop(0),i))



print "sorted output"
while len(heap) > 0:
	x, listindex = heappop(heap)
	print x, listindex
	if len(lists[listindex]) > 0:
		heappush(heap, (lists[listindex].pop(0),listindex))


