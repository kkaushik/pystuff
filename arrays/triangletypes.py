def getTriangletype(a,b,c):
	sides = [a,b,c]
	map = {}
	for side in sides:
		if side > 0:
			map[side] = 1
		else:
			return 4
	maxside=max(sides)	
	sums=sum(sides)  
	if not sums > 2*maxside:
		return 4
	if len(map) == 2:
		return 2
	elif len(map) == 1:
		return 3
	else:
		return 1

	print maxside, sums




print "test cases"
assert getTriangletype(1,2,3) == 4 , "invalid sides"
assert getTriangletype(1,3,3) == 2 , "isoscles triangle"
assert getTriangletype(3,2,4) == 1 , "scalene sides"
assert getTriangletype(0,2,3) == 4 , "invalid sides with zero length"
assert getTriangletype(3,3,3) == 3 , "equilateral sides"




