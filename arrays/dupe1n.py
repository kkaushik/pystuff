
#This program find any number of duplicate elements in an array. Given that the numbers are in range 1 to N-1 and there are N elements in the array.
#Should be done in O(1) constant space and O(N) time.

def main():
  x=0 
  a= [0,1,2,3,4,4,6,6,6,6]
  #a = [1,2,3,1,3,0,6] 
  for i in range(len(a)):
    #print a[i]
    absval = abs(a[i])
    if a[absval] >= 0:
      a[absval] = -a[absval]
    else:
      print "this",absval,"is a dupe"

  print x





if __name__ == "__main__":
  main()
