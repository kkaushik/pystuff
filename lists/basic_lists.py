#! /usr/bin/python
class Node:
  def __init__(self, value, next=None):
    self.value = value
    self.next = next


def addLists(list1, list2, carry):
    if list1==None and list2==None and carry == 0:
        return None

    resultNode = Node(carry)
    value = carry
    if list1 != None:
        value += list1.value
    if list2 != None:
        value += list2.value
    
    resultNode.value = value % 10
    if list1 != None or list2 != None or value != None:
        more = addLists(list1.next if list1 else None, list2.next if list2 else None,
                1 if value >= 10 else 0)
        resultNode.next = more
    return resultNode


def iterReverse(Node):
    currNode = Node
    prevNode = None
    nextNode = None
    while(currNode!=None):
        nextNode = currNode.next
        currNode.next = prevNode
        prevNode = currNode
        currNode = nextNode

    return prevNode




def printRev(Node):
  if Node:
    printRev(Node.next)
    print Node.value


def Reverse(Node):
  if Node==None:
    return None
  elif Node.next == None:
    return Node
  else:
    nextitem = Node.next
    Node.next = None
    reverseRest = Reverse(nextitem)
    nextitem.next = Node
    return reverseRest 
    
def kthfromlast(Node, k):
  ptr1 = Node
  #advance this one k steps ahead, if null just return ptr1
  ptr2 = Node

  for i in range(k):
    if not ptr2:
      return
    ptr2=ptr2.next
  #while ptr2:
  
  #ptr2 = Node.next
  #count=0
  while ptr1 and ptr2:
    ptr1=ptr1.next
    ptr2=ptr2.next
    if ptr2==None:
      return ptr1.value

class Result:
  Node

def kthfromlastrec(Node, k):
  if Node == None:
    return None, 0
  

  Nodex, count = kthfromlastrec(Node.next, k)
  if Nodex == None:
    count += 1
    if count == k:
      Nodex = Node
  return Nodex, count
  
  

def midpoint(Node, count,len):
  if Node.next == None:
    return None, 0, len
  len +=1
  Nodex, count, length = midpoint(Node.next, count,len)
  if Nodex == None:
    count +=1
    if count == length/2:
      Nodex = Node
  return Nodex, count, len


def printList(Node):
  
 while Node:
   print Node.value
   Node= Node.next
   


def main():
  

 #Construct a list of first 10 nos
 L = Node(0,None)
 first = L
 for i in range(5,15):
   L.next = Node(i,None)
   L = L.next
 

 L2 = Node(0,None)
 first2 = L2
 for i in range(5,8):
   L2.next = Node(i,None)
   L2 = L2.next

 p = addLists(first, first2, 0)
 print "l1",
 printList(first)
 print "l2",
 printList(first2)
 print "result",
 printList(p)
 print "result ended"
 #Print the list

 Nodez = first
 printList(Nodez)

 print "before rever"
 printList(first)
 print "After reversing printing"
 printRev(first)
 #Reverse the linked List
 #newfirst = iterReverse(first)
 #printList(newfirst) 
 #recursively?
 
 print "2nd from last element in list"
 #printList(first)
 #print  kthfromlast(first,3)
 print "Midpoint"
 Nodex, m, l =  midpoint(first,0,0)
 print Nodex.value
 # x, y = kthfromlastrec(first, 2)
 #print x.value 
 #printlist( 
 #printList(newfirst)

 







if __name__ == "__main__":
  main()
