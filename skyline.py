#Implementing a quick heap in python and using it to solve the skyline problem



def skyline( buildings):
    if len(buildings)==1:
        keypoints = [[buildings[0][0] ,buildings[0][2]], [buildings[0][1],0]]
        return keypoints
    keypoints = []

    s1 = skyline(buildings[0:len(buildings)/2])
    s2 = skyline(buildings[len(buildings)/2:])

    hta = 0
    htb = 0

    while len(s1) != 0 and len(s2) !=0:
        if s1[0][0] < s2[0][0]:
            currentx = s1[0][0]
            hta = s1[0][1]
            keypoints.append([currentx,max(hta, htb)])
            s1.pop(0)
        else:
            currentx = s2[0][0]
            htb = s2[0][1]
            keypoints.append([currentx,max(hta, htb)])
            s2.pop(0)

        if len(s1) == 0:
            keypoints += s1
        else:
            keypoints += s2
        return keypoints



buildings = [[1,4,2],[2,3,6],[4,5,10],[6,8,13]]


print skyline(buildings)




