__author__ = 'karan'

def isMatch(s, p):
    if len(p) == 0:
        return len(s) == 0

    if (len(p) == 1 or p[1] != '*'):
        if len(s) < 1 or p[0] != '.' and s[0] != p[0]:
            return False
        return isMatch(s[1:],p[1:])

    else:
        lent = len(s)
        i = -1
        while(i < lent and (i<0 or p[0] == '.' or p[0] == s[i])):
            if isMatch(s[i+1:],p[2:]):
                return True
            i+=1
    return False




if __name__ == "__main__":
    print "HELLO"
    print isMatch('abc','ab*c*')
