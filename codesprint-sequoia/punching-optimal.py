# optimal number of times a boxer must punch a punching bag to deflate all.
# Question details are in folder
import sys

def update_score(input, scores):
  for i in range(len(input)):
        
            scores[i] = 0
            #print "Updating", scores[i]
            if i-1 >= 0 and i+1 < len(input):
              scores[i] += input[i-1] + input[i+1]
            elif i-1 >= 0:
              scores[i] += input[i-1]
            elif i+1 < len(input):
              scores[i] += input[i+1]
          
def find_largest_idx(scores):
    maxscore = -1
    maxidx = -1
    for idx in range(len(scores)):
        if scores[idx] > maxscore:
            maxscore = scores[idx]
            maxidx = idx

    return maxidx

def knock(input,idx):
    if idx-1 >= 0 and input[idx-1] > 0:
        input[idx-1]-=1
    
    if idx+1 < len(input) and input[idx+1] >0:
        input[idx+1]-=1
    input[idx]=0
	  
 
def done(input):
  if sum(x > 0 for x in input) > 0:
    return False
  return True
		
		
           
def main():
  input = list(raw_input().rstrip())
  input = [int(item) for item in input]
  scores = [0 for item in input]  
        
  count = 0

  while not done(input):
    #Update all scores
    update_score(input,scores)
    #Find largest idx
    idx = find_largest_idx(scores)
    #Knock that index
    knock(input, idx)
    count+=1
        
  print count
   

if __name__ == "__main__":
  main()
