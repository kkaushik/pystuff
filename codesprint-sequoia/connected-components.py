# we add edges then remove them and show number of connected components as out#put
def walk(G,s, S = set()):
    P, Q = dict(), set()
    P[s] = None
    Q.add(s)
    while Q:
        u = Q.pop()
        for v in G[u].difference(P,S):
            Q.add(v)
            P[v] = u
    return P

def get_neighborhood_count(G):
    comp = []
    seen = set()
    for u in G:
        if u in seen: continue
        C = walk(G,u)
        seen.update(C)
        comp.append(C)
    return len(comp)
    
#Constructing the graph N= Nodes, M = edges
input = raw_input()
N = input.split()[0]
M = input.split()[1]

current_graph = {}
#Build adjacency list representation of graph
removed_edges = []

for edges in range(int(M)):
    input = raw_input()
    src = int(input.split()[0])
    dest = int(input.split()[1])
    removed_edges.append([src,dest])
    current_graph.setdefault(src, set() ).add(dest)
    current_graph.setdefault(dest, set() ).add(src)
    
for edges in range(int(M)):
    src, dest = removed_edges.pop(0)
    current_graph[src].remove(dest)
    current_graph[dest].remove(src)
    print get_neighborhood_count(current_graph)
