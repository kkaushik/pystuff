
def backtrack(r,c,m,n):
    if r == m and c ==n:
        return 1
    if r>m or c>n:
        return 0

    return backtrack(r+1,c,m,n) + backtrack(r,c+1,m,n)

#attempt to print the paths

def backtrackz(r,c,m,n,matrix):
    if matrix[r][c] == -1:
        return 0
    if r == m and c ==n:
        matrix[r][c] = 1
        return 1
    if r>m or c>n:
        return 0

    matrix[r][c] = 1
    if backtrackz(r+1,c,m,n,matrix) == 1:
        return 1
    
    if backtrackz(r,c+1,m,n,matrix) == 1:
        return 1
    matrix[r][c] = 0


matrix = [[0,0,0,0],
        [0,-1,0,0],
        [-1,0,0,0],
        [0,0,0,0]
        ]

print  backtrackz(0,0,2,2,matrix)

#path from starting to ending point, can also block paths with -1
for i in matrix:
    print i
    

