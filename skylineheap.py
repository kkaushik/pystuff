#Implementing a quick heap in python and using it to solve the skyline problem



class Heap:

    def __init__(self,N):
        #Create a heap of size N, with 0th position unused (for storing xtra)
        self.list = [0 for item in range(0,N+1)]
        self.index = 0

    def less(self, a , b):
        if self.list[a]<self.list[b]:
            return True
        else:
            return False

    def insert(self, item):
        self.index+=1
        self.list[self.index] = item
        self.swim(self.index)


    def delMax(self):
        maxx = self.list[1]
        self.list[1], self.list[self.index] = self.list[self.index] , self.list[1]
        self.index -=1
        self.list[self.index+1] = None
        self.sink(1)
        print "lis", self.list
        return maxx
        

    
    def swim(self, k):
        while( k>1 and self.less(k/2, k)):
            self.list[k], self.list[k/2] = self.list[k/2],self.list[k]
            k = k/2

    def sink(self,  k):

        while (2*k <=self.index):

            j = 2*k
            if j<self.index and self.less(j,j+1): 
                j+=1
            if not self.less(k,j): 
                break
            self.list[k], self.list[j] = self.list[j], self.list[k]
            k=j


def skyline( buildings, points):
    pass #if 



heap = Heap(10)

listsam = [2,99,100,222,3,4,5]

for i in range(len(listsam)):
    heap.insert(listsam[i])

for i in range(len(listsam)):
    print heap.delMax()


skyline = []
skypts = [[1,4,1],[4,8,5],[5,12,2],[9,14,8]]


for i in range(len(skypts)):
    current = skypts[i]
    #take each x coordinate start pt
    maxht = current[2]
    for B in skypts:
        if B!=current:
            if B[0] <= current[0] and current[0] <= B[1] and B[2] > maxht:
                maxht = B[2]
    skyline.append([current[0],maxht])


#print skyline
#magnus hietland approach-


skypts = [[1,1,4],[4,5,8],[5,2,12],[9,8,14]]
skyline = []

print "stuff here"

s1 = [[1,2],[5,6],[7,100]]
s2 = [[0,0],[3,6],[6,2]]

hta = 0
htb = 0

while len(s1) != 0 and len(s2) !=0:
    if s1[0][0] < s2[0][0]:
        currentx = s1[0][0]
        hta = s1[0][1]
        print currentx,max(hta, htb)
        s1.pop(0)
    else:
        currentx = s2[0][0]
        htb = s2[0][1]
        print currentx,max(hta, htb)
        s2.pop(0)

if len(s1) == 0:
    print s2
else:
    print s1






