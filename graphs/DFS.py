# Do a DFS on the graph, and print its levels

def DFS(G, s):
    S, Q = set(), []
    Q.append(s)
    while Q:
        u = Q.pop()
        if u in S: continue
        S.add(u)
        Q.extend(G[u])
        yield u


def rec_DFS(G, s, S=None):
    if S is None: S = set()
    S.add(s)
    for u in G[s]:
        print u
        if u in S: continue
        rec_DFS(G,u, S)


#def dfs_toposort(G):
#    S, res = set(), []


def timestamp_DFS(G, s, d, f , t=0, S=None):
    #S, Q = set(), []
    if S is None: S = set()
    d[s] = t; t+=1
    S.add(s)
    for u in G[s]:
        if u in S: continue
        t = timestamp_DFS(G, u, d, f, t, S)
    f[s] = t ; t+=1
    return t


        





G = { 'a' : ['b','c'],
        'b' : ['e','f'],
        'c': ['g','h'],
        'd': [],
        'e': [],
        'f': [],
        'g': [],
        'h': []
        }


#for item in DFS(G, 'a'):
#    print item
#S = set()
#rec_DFS(G, 'a', S)
#print 'a'



d= {}
f = {}
timestamp_DFS(G, 'a', d,f )
print d, f



for key,value in sorted(d.iteritems(), key = lambda (k,v) : (v,k)):
    print key, value


