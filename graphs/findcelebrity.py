#Finding the celebrity among a Group of people
#where a celeb is defined as someone who knows nobody but everyone knows the
#celeb
def naive_celeb(G):
  n = len(G)

  for u in range(n):
      for v in range(n):
          if u == v: continue
          if G[u][v]: break
          if not G[v][u]: break
      else:
          return u
  return None    

def optimal_celeb(G):
    n = len(G)
    u,v = 0,1
    cnt = 2
    while (u < n and v < n):
        if G[u][v]:
            u= cnt
            cnt +=1
        else:
            v = cnt
            cnt+=1
    #More cool but unreadable way to do this
    #for c in range(2,n+1):
    #    if G[u][v]: u = c
    #    else: v = c
    if u==n:
        c=v
    else:
        c = u
    for other in range(n):
        if other==c: continue
        if G[c][other]: break
        if not G[other][c]: break
    else:
        return c
    return None


#celebrity problem, find the celeb

G = [[0,0,1,0],
        [0,0,1,1],
        [0,0,0,0],
        [1,0,1,0]]



print naive_celeb(G)

print optimal_celeb(G)
