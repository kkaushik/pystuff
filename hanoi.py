

def hanoi(x, src, aux, dest):
  if x>0:
    hanoi(x-1, src, dest, aux)
    print "Moved",x,"from",src,"to",dest,"via",aux
    hanoi(x-1, aux, src, dest)

 
 
def main():
  hanoi(3,"src","aux","dest")







if __name__ == "__main__":
  main()
