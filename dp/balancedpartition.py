# Just memoize this guy and you are good to go.. 
# as mentioned in this quora post - http://www.quora.com/Dynamic-Programming/Are-there-any-good-resources-or-tutorials-for-Dynamic-Programming-besides-TopCoder-tutorial
# remove redundant arguments to function, subproblem can be cached


def part(list, l1,l2,l1sum,l2sum):
	if len(list) > 0:
		elem = list[0]
		if abs((l1sum + elem) - l2sum) < abs(l1sum - (l2sum+elem)):
			l1.append(elem)
			return part(list[1:],l1,l2,l1sum+elem,l2sum)
		else:
			l2.append(elem)
			return part(list[1:],l1,l2,l1sum,l2sum+elem)

	else:
		return l1,l2





a= []
b= []
input = [2, 10, 3, 8, 5, 7, 9, 5, 3, 2]
print part(input,a,b,0,0)

