#this version is straight up recursion, needs to be memoized to be real DP
#so that overlapping solutions can be shared among problems.
#can do it iteratively, as mentioned below

def LEC(list, subseq = 0, soln = []):
  if len(list) > 0:
    elem = list[0]
    if subseq + elem > subseq:
      subseq = subseq + elem
      soln.append(elem)
      return LEC(list[1:],subseq,soln)
    else:
      return LEC(list[1:],subseq,soln)
  else:
    return soln






def max(a,b):
  if a>=b:
    return a
  else:
    return b


def main():
  num = [-1,-2,3,3,5,-1,-3,-2,1]
  
  oldmax = 0
  newmax = 0
  for item in num:
    newmax = max(oldmax+item, oldmax)
    oldmax = newmax
  print newmax 





if __name__ == "__main__":
  print LEC([1,-2,2,3,4,-6,-3,1])
  #main()

