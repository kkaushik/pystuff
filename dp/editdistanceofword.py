#Given a dictionary of N words and another word W, find the word
# in the dictionary closest to W. Measure of closeness is no of operations

def edit_distance(word,target):

    cols = len(word)+ 1
    rows = len(target) + 1

    currentRow = [0]

    for column in range(1,cols):
        currentRow.append(currentRow[column-1]+1)

    for row in range(1,rows):
        previousRow = currentRow
        currentRow = [previousRow[0] + 1]

        for column in range(1,cols):

            insertCost = currentRow[column-1] + 1
            deleteCost = previousRow[column] + 1

            if word[column-1] == target[row-1]:
                replaceCost = previousRow[column-1]
            else:
                replaceCost = previousRow[column-1]+1

            currentRow.append(min(insertCost,deleteCost,replaceCost))

    return currentRow[-1]


if __name__ == "__main__":
    print "Edit distance algorithm"
    print edit_distance("ABC","ABC")