# A man climbs n stairs, he can climb only one or two stairs.
# in how many ways can he climb n stairs
from functools import wraps

def memo(func):
    cache = {}
    @wraps(func)
    def wrap(*args):
        if args not in cache:
            cache[args] = func(*args)
        return cache[args]
    return wrap

@memo
def climbsteps(n):
    if n == 0:
        return 1
    if n < 0:
        return 0

    return climbsteps(n-2) + climbsteps(n-1)


print climbsteps(4)
