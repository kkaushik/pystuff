class Solution:
    def _isPalindrome(self, s):
        begin, end = 0, len(s)-1
        while begin < end:
            if s[begin] != s[end]:
                return False
            else:
                begin += 1
                end -= 1
        
        return True

    def partition(self, s):
        if len(s) == 0:     return []
        if len(s) == 1:     return [[s]]
        
        result = []
        if self._isPalindrome(s):   result.append([s])
        
        for i in xrange(1, len(s)):
            head = s[:i]
            if not self._isPalindrome(head):
                continue
            
            tailPartition = self.partition(s[i:])
            x = [[head] + item for item in tailPartition]
            
            result.extend(x)
        
        return result

if __name__ == "__main__":
  x = Solution()
  y = x.partition("abacdxrx")
  z = [(item, len(item)) for item in y ]
  w = sorted(z,key=lambda x: x[1])
  print sum([1 for item in w[0][0] if len(item) == 1])
     

 


